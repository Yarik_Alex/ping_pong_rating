using FluentMigrator;

namespace PingPongRating.Server.Migrator.Migrations
{
    [Migration(1)]
    public class Init : Migration
    {
        public override void Up()
        {
            const string sql = @"
                 create table if not exists users
                 (
                     id serial not null constraint users_pk primary key,
                     name text not null,
                     surname text not null,
                     login text not null,
                     password text not null
                 );

                alter table users
                    add constraint users_login_unique unique (login);

                 create table if not exists players
                 (
                     id serial not null constraint players_pk primary key,
                     user_id integer not null,
                     rating integer not null,
                     matches integer not null,
                     matches_won integer not null,
                     matches_lost integer not null,
                     games_won integer not null,
                     games_lost integer not null,
                     start_date timestamp not null
                 );";
            
            Execute.Sql(sql);
        }

        public override void Down()
        {
            const string sql = @"
                    drop table if exists users;
                    drop table if exists players;";
            
            Execute.Sql(sql);
        }
    }
}