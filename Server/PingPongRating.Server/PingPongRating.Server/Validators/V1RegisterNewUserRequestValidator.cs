using FluentValidation;
using PingPongRating.Server.Models.Requests;

namespace PingPongRating.Server.Validators
{
    public class V1RegisterNewUserRequestValidator : AbstractValidator<V1RegisterNewUserRequest>
    {
        public V1RegisterNewUserRequestValidator()
        {
            RuleFor(x => x.Name)
                .NotNull()
                .NotEmpty()
                .Must(x => x.All(char.IsLetter))
                .WithMessage("Name can contain only lettres.");

            RuleFor(x => x.Surname)
                .NotNull()
                .NotEmpty()
                .Must(x => x.All(char.IsLetter))
                .WithMessage("Name can contain only lettres.");;

            RuleFor(x => x.Login)
                .NotNull()
                .NotEmpty();

            RuleFor(x => x.Password)
                .NotNull()
                .NotEmpty()
                .MinimumLength(6)
                .WithMessage("Lenght must be great than 6 symbols.");
        }
    }
}