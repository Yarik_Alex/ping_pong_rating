using FluentValidation;
using PingPongRating.Server.Models.Requests;

namespace PingPongRating.Server.Validators
{
    public class V1AuthenticateUserRequestValidator : AbstractValidator<V1AuthenticateUserRequest>
    {
        public V1AuthenticateUserRequestValidator()
        {
            RuleFor(x => x.Login)
                .NotNull()
                .NotEmpty()
                .WithMessage("Login can't be empty.");
            
            RuleFor(x => x.Password)
                .NotNull()
                .NotEmpty()
                .WithMessage("Password can't be empty.");
        }
    }
}