namespace PingPongRating.Server.Models
{
    public enum ErrorCodes
    {
        UserAlreadyExists = 10,
        UserNotFound,
    }
}