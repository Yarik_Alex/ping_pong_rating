namespace PingPongRating.Server.Models.Responses
{
    public class V1RegisterNewUserResponse
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public string Surname { get; set; }
    }
}