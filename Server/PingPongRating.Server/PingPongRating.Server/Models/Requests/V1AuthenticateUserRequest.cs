namespace PingPongRating.Server.Models.Requests
{
    public class V1AuthenticateUserRequest
    {
        public string Login { get; set; }

        public string Password { get; set; }
    }
}