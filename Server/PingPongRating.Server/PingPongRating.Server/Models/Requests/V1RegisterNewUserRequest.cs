namespace PingPongRating.Server.Models.Requests
{
    public class V1RegisterNewUserRequest
    {
        public string Name { get; set; }
        
        public string Surname { get; set; }
        
        public string Login { get; set; }
        
        public string Password { get; set; }
    }
}