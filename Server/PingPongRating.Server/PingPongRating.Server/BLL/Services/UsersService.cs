using System.Security.Authentication;
using PingPongRating.Server.BLL.Models;
using PingPongRating.Server.BLL.Services.Interfaces;
using PingPongRating.Server.DAL.Interfaces;
using PingPongRating.Server.DAL.Models.Users;
using System.Security.Cryptography;
using System.Text;
using PingPongRating.Server.BLL.Exceptions;

namespace PingPongRating.Server.BLL.Services
{
    public class UsersService : IUsersService
    {
        private readonly IUsersRepository _usersRepository;

        public UsersService(
            IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public async Task<UserModel> RegisterUser(RegisterUserModel model, CancellationToken token)
        {
            var encodedPassword = EncodingPassword(model.Password);
            
            var response = await _usersRepository.Insert(
                new InsertUserModel
                {
                    UserName = model.UserName,
                    UserSurname = model.UserSurname,
                    Login = model.Login,
                    Password = encodedPassword
                },
                token);

            if (!response.Any())
            {
                throw new UserAlreadyExistsException(model.Login);
            }

            var user = response.First();

            return new UserModel
            {
                Id = user.Id,
                Name = user.Name,
                Surname = user.Surname
            };
        }

        public async Task<UserModel> AuthenticateUser(AuthenticateUserModel model, CancellationToken token)
        {
            var encodedPassword = EncodingPassword(model.Password);

            var response = await _usersRepository.Select(
                new SelectUserModel
                {
                    Login = model.Login,
                    Password = encodedPassword
                },
                token);
            
            if (!response.Any())
            {
                throw new UserNotFoundException(model.Login);
            }

            var user = response.First();
            
            return new UserModel
            {
                Id = user.Id,
                Name = user.Name,
                Surname = user.Surname
            };
        }

        private string EncodingPassword(string password)
        {
            var md5 = MD5.Create();

            var bytes = Encoding.ASCII.GetBytes(password);
            var hash = md5.ComputeHash(bytes);

            var stringBuilder = new StringBuilder();
            foreach (var element in hash)
            {
                stringBuilder.Append(element.ToString("X2"));
            }

            return stringBuilder.ToString();
        }
    }
}