using PingPongRating.Server.BLL.Models;

namespace PingPongRating.Server.BLL.Services.Interfaces
{
    public interface IUsersService
    {
        Task<UserModel> RegisterUser(RegisterUserModel model, CancellationToken token);

        Task<UserModel> AuthenticateUser(AuthenticateUserModel model, CancellationToken token);
    }
}