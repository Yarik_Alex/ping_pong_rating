namespace PingPongRating.Server.BLL.Models
{
    public class RegisterUserModel
    {
        public string UserName { get; set; }
        public string UserSurname { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}