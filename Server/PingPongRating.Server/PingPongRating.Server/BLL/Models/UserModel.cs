namespace PingPongRating.Server.BLL.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public string Surname { get; set; }
    }
}