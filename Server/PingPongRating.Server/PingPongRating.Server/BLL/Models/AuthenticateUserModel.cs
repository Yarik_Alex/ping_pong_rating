namespace PingPongRating.Server.BLL.Models
{
    public class AuthenticateUserModel
    {
        public string Login { get; set; }

        public string Password { get; set; }
    }
}