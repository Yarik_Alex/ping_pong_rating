using PingPongRating.Server.Models;

namespace PingPongRating.Server.BLL.Exceptions
{
    public class UserAlreadyExistsException : PingPongRatingExceptionBase
    {
        public string Login { get; set; }

        public UserAlreadyExistsException(string login)
        {
            Login = login;
            Code = ErrorCodes.UserAlreadyExists;
            Message = $"User with login {login} already exists.";
            Details = new { login };
        }
    }
}