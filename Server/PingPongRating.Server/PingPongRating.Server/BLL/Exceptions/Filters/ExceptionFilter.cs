using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace PingPongRating.Server.BLL.Exceptions.Filters
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception is PingPongRatingExceptionBase exception)
            {
                WriteError(context, exception);
            }
        }
        
        private static void WriteError(ExceptionContext context, PingPongRatingExceptionBase exception)
        {
            context.Result = new ObjectResult(new
            {
                exception.Code,
                exception.Message,
                exception.Details
            }) { StatusCode = 400 };
        }
    }
}