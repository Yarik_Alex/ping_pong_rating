using PingPongRating.Server.Models;

namespace PingPongRating.Server.BLL.Exceptions
{
    public class UserNotFoundException : PingPongRatingExceptionBase
    {
        public string Login { get; set; }

        public UserNotFoundException(string login)
        {
            Login = login;
            Code = ErrorCodes.UserNotFound;
            Message = $"User with login {login} not found.";
            Details = new { login };
        }
    }
}