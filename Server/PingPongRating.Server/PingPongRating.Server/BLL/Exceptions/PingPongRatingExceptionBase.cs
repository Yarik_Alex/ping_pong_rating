using PingPongRating.Server.Models;

namespace PingPongRating.Server.BLL.Exceptions
{
    public class PingPongRatingExceptionBase : Exception
    {
        public ErrorCodes Code { get; set; }
        public string Message { get; set; }
        public object Details { get; set; }

        public PingPongRatingExceptionBase()
        {
            
        }

        public PingPongRatingExceptionBase(
            string message,
            object details,
            ErrorCodes code)
        {
            Message = message;
            Details = details;
            Code = code;
        }
    }
}