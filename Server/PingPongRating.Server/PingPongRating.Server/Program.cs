using PingPongRating.Server.BLL.Services;
using PingPongRating.Server.BLL.Services.Interfaces;
using PingPongRating.Server.DAL.Interfaces;
using PingPongRating.Server.DAL.Repositories;
using FluentValidation.AspNetCore;
using PingPongRating.Server.BLL.Exceptions.Filters;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddControllers(x => x.Filters.Add<ExceptionFilter>())
    .AddFluentValidation(
        configurationExpression: x =>
        {
            x.ImplicitlyValidateChildProperties = true;
            x.RegisterValidatorsFromAssemblies(AppDomain.CurrentDomain.GetAssemblies());
        });

//Repositories
builder.Services.AddScoped<IUsersRepository, UsersRepository>();

//Services
builder.Services.AddScoped<IUsersService, UsersService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapControllers();

app.Run();