using Microsoft.AspNetCore.Mvc;
using PingPongRating.Server.BLL.Models;
using PingPongRating.Server.BLL.Services.Interfaces;
using PingPongRating.Server.Models.Requests;
using PingPongRating.Server.Models.Responses;

namespace PingPongRating.Server.Controllers.Users
{
    [Route("Users")]
    [ApiController]
    public class UsersController : Controller
    {
        private readonly IUsersService _usersService;

        public UsersController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [HttpPost("registration")]
        public async Task<ActionResult<V1RegisterNewUserResponse>> RegisterNewUser(
            V1RegisterNewUserRequest request,
            CancellationToken token)
        {
            var user = await _usersService.RegisterUser(
                new RegisterUserModel
                {
                    UserSurname = request.Surname,
                    UserName = request.Name,
                    Login = request.Login,
                    Password = request.Password
                },
                token);

            var response = new V1RegisterNewUserResponse
            {
                Id = user.Id,
                Name = user.Name,
                Surname = user.Surname
            };

            return Ok(response);
        }

        [HttpPost ("authentication")]
        public async Task<ActionResult<V1AuthenticateUserResponse>> AuthenticateUser(
            V1AuthenticateUserRequest request, 
            CancellationToken token)
        {
            var user = await _usersService.AuthenticateUser(
                new AuthenticateUserModel
                {
                    Login = request.Login,
                    Password = request.Password
                },
                token);

            var response = new V1AuthenticateUserResponse
            {
                Id = user.Id,
                Name = user.Name,
                Surname = user.Surname
            };

            return Ok(response);
        }
    }
}