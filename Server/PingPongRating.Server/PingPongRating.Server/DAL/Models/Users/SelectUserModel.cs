namespace PingPongRating.Server.DAL.Models.Users
{
    public class SelectUserModel
    {
        public string Login { get; set; }

        public string Password { get; set; }
    }
}