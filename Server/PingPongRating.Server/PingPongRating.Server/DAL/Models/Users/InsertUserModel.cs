namespace PingPongRating.Server.DAL.Models.Users
{
    public class InsertUserModel
    {
        public string UserName { get; set; }
        
        public string UserSurname { get; set; }
        
        public string Login { get; set; }
        
        public string Password { get; set; }
    }
}