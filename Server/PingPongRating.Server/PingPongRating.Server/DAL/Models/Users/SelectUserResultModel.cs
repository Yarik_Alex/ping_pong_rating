namespace PingPongRating.Server.DAL.Models.Users
{
    public class SelectUserResultModel
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public string Surname { get; set; }
    }
}