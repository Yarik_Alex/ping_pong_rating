namespace PingPongRating.Server.DAL.Models.Users
{
    public class InsertUserResultModel
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public string Surname { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }
    }
}