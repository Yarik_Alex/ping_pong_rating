namespace PingPongRating.Server.DAL.Models.Users
{
    public class UpdateUserModel
    {
        public string UserName { get; set; }

        public string UserSurname { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }
    }
}