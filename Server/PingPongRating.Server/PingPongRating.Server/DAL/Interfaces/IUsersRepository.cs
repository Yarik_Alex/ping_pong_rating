using PingPongRating.Server.DAL.Models.Users;

namespace PingPongRating.Server.DAL.Interfaces
{
    public interface IUsersRepository
    {
        Task<List<InsertUserResultModel>> Insert(InsertUserModel model, CancellationToken token);

        // Task<UpdateUserResultModel> Update(UpdateUserModel model, CancellationToken token);
        //
        Task<List<SelectUserResultModel>> Select(SelectUserModel model, CancellationToken token);
        //
        // Task<DeleteUserResultModel> Delete(DeleteUserModel model, CancellationToken token);
    }
}