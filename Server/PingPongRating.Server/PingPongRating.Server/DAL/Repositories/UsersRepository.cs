using Dapper;
using PingPongRating.Server.DAL.Interfaces;
using PingPongRating.Server.DAL.Models.Users;

namespace PingPongRating.Server.DAL.Repositories
{
    public class UsersRepository : BaseRepository, IUsersRepository
    {
        public async Task<List<InsertUserResultModel>> Insert(
            InsertUserModel model,
            CancellationToken token)
        {
            var sql = @"insert into users (name, surname, login, password)
                         values (@Name, @Surname, @Login, @Password)
                         on conflict on constraint users_login_unique
                         do nothing
                         returning id, name, surname, login, password";

            var param = new
            {
                Name = model.UserName,
                Surname = model.UserSurname,
                Login = model.Login,
                Password = model.Password
            };

            var connection = await GetConnectionAsync();
            var response = await connection.QueryAsync<InsertUserResultModel>(sql, param);

            return response.ToList();
        }

        public async Task<List<SelectUserResultModel>> Select(SelectUserModel model, CancellationToken token)
        {
            var sql = @"select id, name, surname from users
                        where login = @Login and password = @Password;";

            var param = new
            {
                Login = model.Login,
                Password = model.Password
            };

            var connection = await GetConnectionAsync();
            var response = await connection.QueryAsync<SelectUserResultModel>(sql, param);

            return response.ToList();
        }
    }
}