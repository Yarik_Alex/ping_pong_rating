using Npgsql;

namespace PingPongRating.Server.DAL.Repositories
{
    public abstract class BaseRepository
    {
        protected async Task<NpgsqlConnection> GetConnectionAsync()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();

            var connectionString = builder.GetConnectionString("ServerDatabase");

            return new NpgsqlConnection(connectionString);
        }
    }
}